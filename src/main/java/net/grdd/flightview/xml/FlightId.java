//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-b10 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.02.22 at 02:47:55 PM PST 
//


package net.grdd.flightview.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}FlightNumber"/>
 *         &lt;choice>
 *           &lt;element ref="{}CommercialAirline"/>
 *           &lt;element name="GeneralAviation" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element ref="{}CodeShare"/>
 *           &lt;element name="OperatingFlight" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flightNumber",
    "commercialAirline",
    "generalAviation",
    "codeShare",
    "operatingFlight"
})
@XmlRootElement(name = "FlightId")
public class FlightId {

    @XmlElement(name = "FlightNumber", required = true)
    protected String flightNumber;
    @XmlElement(name = "CommercialAirline")
    protected CommercialAirline commercialAirline;
    @XmlElement(name = "GeneralAviation")
    protected Object generalAviation;
    @XmlElement(name = "CodeShare")
    protected CodeShare codeShare;
    @XmlElement(name = "OperatingFlight")
    protected Object operatingFlight;

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the commercialAirline property.
     * 
     * @return
     *     possible object is
     *     {@link CommercialAirline }
     *     
     */
    public CommercialAirline getCommercialAirline() {
        return commercialAirline;
    }

    /**
     * Sets the value of the commercialAirline property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialAirline }
     *     
     */
    public void setCommercialAirline(CommercialAirline value) {
        this.commercialAirline = value;
    }

    /**
     * Gets the value of the generalAviation property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getGeneralAviation() {
        return generalAviation;
    }

    /**
     * Sets the value of the generalAviation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setGeneralAviation(Object value) {
        this.generalAviation = value;
    }

    /**
     * Gets the value of the codeShare property.
     * 
     * @return
     *     possible object is
     *     {@link CodeShare }
     *     
     */
    public CodeShare getCodeShare() {
        return codeShare;
    }

    /**
     * Sets the value of the codeShare property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeShare }
     *     
     */
    public void setCodeShare(CodeShare value) {
        this.codeShare = value;
    }

    /**
     * Gets the value of the operatingFlight property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getOperatingFlight() {
        return operatingFlight;
    }

    /**
     * Sets the value of the operatingFlight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setOperatingFlight(Object value) {
        this.operatingFlight = value;
    }

}
