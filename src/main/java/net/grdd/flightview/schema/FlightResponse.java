/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.flightview.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * Flight Response
 * @author samiruppaluru
 * @since Feb 22, 2012
 */
@SuppressWarnings("serial")
@JsonSerialize(include = Inclusion.NON_NULL)
public class FlightResponse implements Serializable {

	private Boolean response;
	private String message;
	private List<Flight> flights = new ArrayList<Flight>();
	private ErrorResponse error;
	
	public Boolean getResponse() {
		return response;
	}
	public void setResponse(Boolean response) {
		this.response = response;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Flight> getFlights() {
		return flights;
	}
	public void setFlights(List<Flight> flights) {
		this.flights = flights;
	}
	public ErrorResponse getError() {
		return error;
	}
	public void setError(ErrorResponse error) {
		this.error = error;
	}
	@Override
	public String toString() {
		return "FlightResponse [flights=" + flights + ", response=" + response
				+ ", message=" + message + ", error=" + error + "]";
	}
	
	
}
