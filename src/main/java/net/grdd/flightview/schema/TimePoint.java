/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.flightview.schema;

import java.util.Date;

/**
 * Flight Segment Time Point
 * @author samiruppaluru
 * @since Feb 15, 2012
 */
public class TimePoint {

	private String status;
	private String time;
	
	public String getStatus() {
		return status;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
