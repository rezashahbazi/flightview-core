/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.flightview.schema;

/**
 * Flight Segment End Point
 * @author samiruppaluru
 * @since Feb 15, 2012
 */
public class EndPoint {

	private String airportCode;
	private String airportName;
	private TimePoint scheduled;
	private TimePoint current;
	private String terminal;
	private String gate;
	private String baggage;
	
	public String getAirportCode() {
		return airportCode;
	}
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
	public String getAirportName() {
		return airportName;
	}
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	public TimePoint getScheduled() {
		return scheduled;
	}
	public void setScheduled(TimePoint scheduled) {
		this.scheduled = scheduled;
	}
	public TimePoint getCurrent() {
		return current;
	}
	public void setCurrent(TimePoint current) {
		this.current = current;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public String getGate() {
		return gate;
	}
	public void setGate(String gate) {
		this.gate = gate;
	}
	public String getBaggage() {
		return baggage;
	}
	public void setBaggage(String baggage) {
		this.baggage = baggage;
	}
	
}
