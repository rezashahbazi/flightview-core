/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.flightview.schema;

/**
 * Flight ID
 * @author samiruppaluru
 * @since Feb 15, 2012
 */

public class Flight {

	private String timestamp;
	private FlightId flightId;
	private String flightStatus;
	private FlightId codeShareId;
	private String codeType;
	private EndPoint departure;
	private EndPoint arrival;
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public FlightId getFlightId() {
		return flightId;
	}
	public void setFlightId(FlightId flightId) {
		this.flightId = flightId;
	}
	public String getFlightStatus() {
		return flightStatus;
	}
	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}
	public FlightId getCodeShareId() {
		return codeShareId;
	}
	public void setCodeShareId(FlightId codeShareId) {
		this.codeShareId = codeShareId;
	}
	public String getCodeType() {
		return codeType;
	}
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	public EndPoint getDeparture() {
		return departure;
	}
	public void setDeparture(EndPoint departure) {
		this.departure = departure;
	}
	public EndPoint getArrival() {
		return arrival;
	}
	public void setArrival(EndPoint arrival) {
		this.arrival = arrival;
	}
	@Override
	public String toString() {
		return "Flight [flightId=" + flightId + ", flightStatus="
				+ flightStatus + ", timestamp=" + timestamp + ", codeType="
				+ codeType + ", departure=" + departure + ", arrival="
				+ arrival + ", codeShareId=" + codeShareId + "]";
	}
	
}
